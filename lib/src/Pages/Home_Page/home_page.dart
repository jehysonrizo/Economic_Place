import 'package:economic_place/src/Widgets/custom_navigationbar.dart';
import 'package:flutter/material.dart';

import 'home_page_body.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text(
            'Economic Place',
            style: TextStyle(color: Colors.black),
          ),
          centerTitle: true,
          elevation: 1,
          backgroundColor: Colors.white,
          actions: [
            IconButton(
                onPressed: () {},
                icon: const Icon(
                  Icons.verified_user,
                  color: Colors.amberAccent,
                ))
          ],
        ),
        body: Stack(children: const [
          // BackGround()
          HomePageBody()
        ]),
        bottomNavigationBar: const CustomNavigatorBar(),
      ),
    );
  }
}
