import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../providers/ui_provider.dart';
import '../Categories_Page/categories_page.dart';
import '../User_Page/user_page.dart';

class HomePageBody extends StatelessWidget {
  const HomePageBody({super.key});

  @override
  Widget build(BuildContext context) {
    //OBTENER EL SELECT MENU OPT
    final uiProvider = Provider.of<UiProvider>(context);

    final currentIndex = uiProvider.selectedMenuOpt;
    switch (currentIndex) {
      case 0:
        return const CategoriesPage();
      case 1:
        return const UserPage();
      default:
        return const CategoriesPage();
    }
  }
}
