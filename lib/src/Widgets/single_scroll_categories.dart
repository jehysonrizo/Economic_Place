import 'package:flutter/material.dart';

import 'categories_for_your_company.dart';

class SingleSrollCategories extends StatelessWidget {
  const SingleSrollCategories({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Column(
        children: [
          Row(
            children: const [
              ForYourCompany(
                text: 'Todas las Categorias',
                color: Colors.orange,
                icon: Icons.category,
              ),
              ForYourCompany(
                text: 'Mas Populares',
                color: Colors.blue,
                 icon: Icons.people_outline_sharp,
              ),
              ForYourCompany(
                text: 'Para tu negocio',
                color: Colors.orangeAccent,
                 icon: Icons.view_compact_rounded,
              ),
              ForYourCompany(
                text: 'Rebajas',
                color: Colors.blueAccent,
                 icon: Icons.usb_off_outlined,
              ),
            ],
          )
        ],
      ),
    );
  }
}