import 'package:economic_place/src/Widgets/single_card.dart';
import 'package:economic_place/src/Widgets/single_scroll_categories.dart';
import 'package:flutter/material.dart';

class CardTable extends StatelessWidget {
  const CardTable({super.key});

  @override
  Widget build(BuildContext context) {
    const textStyle = TextStyle(fontSize: 22, fontWeight: FontWeight.bold);
    return SafeArea(
      child: Column(
        children: [
          Row(
            children: const [
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  'Para tu Empresa',
                  style: textStyle,
                ),
              ),
            ],
          ),
          const SingleSrollCategories(),
          Row(
            children: const [
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  'Mejor clasificados',
                  style: textStyle,
                ),
              ),
            ],
          ),
          Table(
            children: const [
              TableRow(children: [
                SingleCard(
                  color: Colors.blue,
                  text: 'General',
                  icon: Icons.border_all,
                ),
                SingleCard(
                  color: Colors.pinkAccent,
                  text: 'Lo mas Vendido',
                  icon: Icons.card_travel,
                ),
              ]),
            ],
          )
        ],
      ),
    );
  }
}
