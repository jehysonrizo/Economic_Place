import 'package:flutter/material.dart';

class ForYourCompany extends StatelessWidget {
  final String text;
  final Color color;
  final IconData icon;
  const ForYourCompany(
      {super.key, required this.text, required this.color, required this.icon});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: color
            ),
            height: 60,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Text(text,
                          style: const TextStyle(color: Colors.white),
                          textAlign: TextAlign.center),
                      Icon(icon, color: Colors.white70,)
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
